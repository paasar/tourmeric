import React from 'react';
import AdminEventList from './AdminEventList-container';

export const PublishedAdminEventList = () => (
  <AdminEventList published showNewEventButton={false} />
);
